<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class CustomerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    
    public function run()
    {
        DB::table('customers')->insert([[
            
                'name' => Str::random(10),
                'email' => 'a@a.com',
                'phone' => random_int(10,1000),
                'user_id'=> 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => Str::random(10),
                'email' => 'b@ab.com',
                'phone' => random_int(10,1000),
                'user_id'=> 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'name' => Str::random(10),
                'email' => 'c@c.com',
                'phone' => random_int(10,1000),
                'user_id'=> 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()                      
                ]]);            
    }
}
