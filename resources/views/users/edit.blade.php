@extends('layouts.app')

@section('title', 'Edit user')

@section('content')       
       <h1>Edit user</h1>
        <form method = "post" action = "{{action('UsersController@update',$user->id)}}">
        @csrf
        @METHOD('PATCH')
        <div class="form-group">
            <label for = "name">User name</label>
            <input type = "text" class="form-control" name = "name" value = {{$user->name}}>
        </div>     
        <div class="form-group">
            <label for = "email">User email</label>
            <input type = "text" class="form-control" name = "email" value = {{$user->email}}>
        </div> 
        <div>
        <div class="form-group">
            <label for = "email">User Department</label>
            <input type = "text" class="form-control" name = "department" value = {{$user->department_id}}>
        </div> 
            <input type = "submit" name = "submit" value = "Update user">
        </div>                       
        </form>    
    </body>
</html>
@endsection
