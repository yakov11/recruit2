@extends('layouts.app')

@section('title', 'candidate info')

@section('content')      

       <h1>users details</h1>
        
    <table class="table table-light" >
    <th>id</th><th>Name</th>></th><th>Email</th><th>department</th>

    @foreach($users as $user)
    @if ($user->ronaldo =='1')
    <tr style="background-color:#98FB98">
    
    @else
        <tr>
    @endif
        
        <td >{{$user->id}}</td>
        <td>{{$user->name}}</td>
        <td>{{$user->email}}</td>
        <td> {{$user->department->name}}</td>

        
        <td>
                <a href = "{{route('users.edit',$user->id)}}">Edit</a>
            </td> 
            <td>
                    <a href = "{{route('user.deleteuser',$user->id)}}">Delete</a>
            </td>
            
            <td>
            @if(Gate::allows('assign-user'))
                @if ($user->ronaldo =='0')
                    <a href = "{{route('user.change',$user->id)}}">deal closed</a>
                @endif
            @else
                <td></td>
            
            @endif
                </td> 
            
             
        </tr>
        @endforeach
        
    </table>
    
@endsection
  

        