@extends('layouts.app')

@section('title', 'candidate info')

@section('content')      

       <h1>candidate details</h1>
        
    <table class = "table table-dark">
    <th>id</th><th>Name</th><th>Age</th></th><th>Email</th><th>Owner</th><th>Status</th><th>Created</th><th>Updated</th>

    
        <tr>
        <td>{{$candidate->id}}</td>
        <td>{{$candidate->name}}</td>
        <td>{{$candidate->age}}</td>
        <td>{{$candidate->email}}</td>
        <td>@if(isset($candidate->user_id))
                {{$candidate->owner->name}}  
            @else
                Assign owner
            @endif</td>
            <td>
                <div class="dropdown">
                    @if (null != App\Status::next($candidate->status_id))    
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        @if (isset($candidate->status_id))
                           {{$candidate->status->name}}
                        @else
                            Define status
                        @endif
                    </button>
                    @else 
                    {{$candidate->status->name}}
                    @endif
                                                   
                    @if (App\Status::next($candidate->status_id) != null )
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                        @foreach(App\Status::next($candidate->status_id) as $status)
                  
                         <a class="dropdown-item" >{{$status->name}}</a>
                        @endforeach                               
                    </div>
                    @endif
                </div>                            
            </td>                             
        
        <td></td>
        </tr>
    </table>
    <a href = "{{route('candidates.changestatus', [$candidate->id,$candidate->id])}}">update status</a>
@endsection
  

        