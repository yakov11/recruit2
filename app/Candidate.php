<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Candidate extends Model
{
    protected $fillable = ['name','email'];

    public function owner(){
        return $this->belongsTo('App\User','user_id');
    }

    public function status()
    {
        return $this->belongsTo('App\Status');
    }     
    public static function userallowed($from){
        $allowed = DB::table('candidates')->where('user_id',$from)->get();
        if(isset($allowed)) return true;
        return false;
    }


}