<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Enumerable;
use App\Department;
use App\Candidate;
use App\User;
use App\Status;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;


class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = new User();
        $user = $user->create($request->all());
        $user->save();
        
        return back()    ;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(Gate::allows('add-user'))
        {
        $user = User::findOrFail($id);
        return view('users.edit', compact('user'));
        }
        else{
            Session::flash('habibi', 'You are gay!!!!!');
            return view('users.gay');
        }
        
    }
    public function usershow()
    {
        $users = User::all();
        $statuses = Status::all(); 
        $departments = Department::all();
        $candidates = Candidate::all();
        
        return view('users.userinfo', compact('candidates','users', 'statuses', 'departments'));       
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $user->update($request->all());
        $users = User::all();
        return view('users.userinfo', compact('users')); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = Candidate::findOrFail($id);
        $user->delete(); 
        return redirect('users.userinfo'); 
    }
    public function change($id)
    {
        Gate::authorize('assign-user');
        $user = User::findOrFail($id);
        $user->ronaldo=1; 
        $user->save();
        $users = User::all();
        return view('users.userinfo', compact('users')); 
    }
}
