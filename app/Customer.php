<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model

public function customers(){
    return $this->belongsTo('App\User','user_id');
}
}